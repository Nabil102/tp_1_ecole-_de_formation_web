class Contact {
  constructor (id, nom, prenom, courriel, telephone, sujet, message) {
    this.id = id
    this.nom = nom
    this.prenom = prenom
    this.courriel = courriel
    this.telephone = telephone
    this.sujet = sujet
    this.message = message
  }
}
const tabcontact = []
exports.addContact = function (data) {
  const id = tabcontact.length
  tabcontact.push(new Contact(id, data.nom, data.prenom, data.courriel, data.telephone, data.sujet, data.message))
  return id
}
