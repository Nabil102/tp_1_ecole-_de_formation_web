class Article {
  constructor (id, auteur, date, titre) {
    this.id = id
    this.auteur = auteur
    this.date = date
    this.titre = titre
  }
}

const data = [
  new Article(0, 'Chris Coyier', '12-03-2021', 'Lorem ipsum dolor sit amet'),
  new Article(1, 'Geoff Graham', '01-03-2021', 'Lorem ipsum dolor sit amet'),
  new Article(2, 'Francis Jemes', '27-02-2021', 'Lorem ipsum dolor sit amet'),
  new Article(3, 'Bader Jalal', '03-02-2021', 'Lorem ipsum dolor sit amet'),
  new Article(4, 'Sarah Drasner', '06-01-2021', 'Lorem ipsum dolor sit amet'),
  new Article(5, 'Philipe Saint-Jaques', '01-01-2021', 'Lorem ipsum dolor sit amet'),
  new Article(6, 'Samson Omojola', '02-02-2020', 'Lorem ipsum dolor sit amet'),
  new Article(7, 'Pierre David', '06-02-2020', 'Lorem ipsum dolor sit amet'),
  new Article(8, 'Boby Talent', '03-01-2020', 'Lorem ipsum dolor sit amet'),
  new Article(9, 'Salim Harri', '12-12-2020', 'Lorem ipsum dolor sit amet'),
  new Article(10, 'Daniel Blanchette', '09-07-2020', 'Lorem ipsum dolor sit amet'),
  new Article(11, 'Isabelle Richer', '08-08-2020', 'Lorem ipsum dolor sit amet')

]

exports.getArticleById = function (id) {
  return data[id]
}

exports.allArticles = data
