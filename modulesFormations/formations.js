class Formation {
  constructor (id, nom, desc, duree, cout, type) {
    this.id = id
    this.nom = nom
    this.desc = desc
    this.duree = duree
    this.cout = cout
    this.type = type
  }
}

const formation = [
  new Formation(0, 'Notions de base MongoDB', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ', '2h30', '60$', 'mongoDB'),
  new Formation(1, 'Installation MongoDB Windows', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ', '1h30', '40$', 'mongoDB'),
  new Formation(2, 'Création serveur MongoDB', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ', '2h00', '70$', 'mongoDB'),
  new Formation(3, 'Ajouter des données sur MongoDB', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ', '1h00', '60$', 'mongoDB'),
  new Formation(4, 'Supprimer des données sur MongoDB', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ', '3h00', '100$', 'mongoDB'),
  new Formation(5, 'Initiation Nodejs', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ', '2h30', '60$', 'nodejs'),
  new Formation(6, 'Installatiom Nodejs sur Windows', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ', '2h30', '60$', 'nodejs'),
  new Formation(7, 'Création serveur Nodejs', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ', '2h30', '60$', 'nodejs'),
  new Formation(8, 'Documentations NodeJs', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ', '2h30', '60$', 'nodejs'),
  new Formation(9, 'Frameworks NodeJs', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ', '2h30', '60$', 'nodejs')

]

exports.getFormationMongoDB = function () {
  const tabMongoDB = []
  for (let i = 0; i < formation.length; i++) {
    if (formation[i].type === 'mongoDB') {
      tabMongoDB.push(formation[i])
    }
  }
  return tabMongoDB
}

exports.getFormationNodjs = function () {
  const tabNodjs = []
  for (let i = 0; i < formation.length; i++) {
    if (formation[i].type === 'nodejs') {
      tabNodjs.push(formation[i])
    }
  }
  return tabNodjs
}

exports.getFormationById = function (id) {
  return formation[id]
}
