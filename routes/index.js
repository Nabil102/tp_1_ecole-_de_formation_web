const express = require('express')
const router = express.Router()
const { check, validationResult } = require('express-validator')
const formations = require('../modulesFormations/formations.js')
const contact = require('../modulesFormations/ajoutContact.js')
const article = require('../modulesFormations/article.js')
const { route } = require('./users.js')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: "Bienvenue à l'école de formation Web" })
})

router.get('/mongoDB', function (req, res, next) {
  res.render('formations/mongoDB', {
    title: 'Les formations MongoDB',
    formation: formations.getFormationMongoDB()
  })
})

router.get('/nodejs', function (req, res, next) {
  res.render('formations/nodejs', {
    title: 'Les formations Nodejs',
    formation: formations.getFormationNodjs()
  })
})

router.get('/blog', function (req, res, next) {
  res.render('blog', { title: 'Blog', article: article.allArticles })
})

router.get('/contact', function (req, res, next) {
  res.render('contact', { data: req.query })
})

router.post('/contact', [
  check('nom', 'Le Champ nom est vide ou invalide')
    .exists()
    .isLength({ min: 3 }),
  check('prenom', 'Le champ prénom est vide ou invalide')
    .exists()
    .isLength({ min: 3 }),
  check('courriel', 'Adresse mail vide ou incorrect').isEmail(),
  check('phone', 'Le numéro de téléphone est invalide').isMobilePhone()
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    const alert = errors.array()
    res.render('contact', {
      alert
    })
  }
  const addNouveauContact = contact.addContact(req.body)
  if (addNouveauContact) {
    res.render('nouveauContact', { title: 'Nous avons bien reçus votre demande : ', confirmation: req.body })
  }
})
router.get('/article/:id', function (req, res, next) {
  res.render('article', { title: 'Blog', article: article.getArticleById(req.params.id) })
})

router.get('/formationMongoDB/:id', function (req, res, next) {
  res.render('formations/formationMongoDB', { title: 'Votre formation MongoDB', formation: formations.getFormationById(req.params.id) })
})

router.get('/formationNodejs/:id', function (req, res, next) {
  res.render('formations/formationNodejs', { title: 'Votre formation NodeJs', formation: formations.getFormationById(req.params.id) })
})

module.exports = router
